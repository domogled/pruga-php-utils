<?php

namespace pruga\utils;

use Monolog\Formatter\JsonFormatter;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\FirePHPHandler;
use Monolog\Handler\PHPConsoleHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

function create_logger(string $logFile): Logger {

  $dirname = dirname($logFile);
  
  if (!is_dir($dirname)) {
      mkdir($dirname, 0700, $recursive = true);
  }


  $logger = new Logger('pruga Ⱂⱃⱆⰳⰰ');
  $fileHandler = new StreamHandler($logFile, Logger::DEBUG);
  $logger->pushHandler($fileHandler);

  // echo "LOGGER LEVEL ---------------------------- " . $fileHandler->getLevel();

  $logger->pushHandler(new StreamHandler('php://stderr', Logger::DEBUG));


  // $output = "dddd [%datetime%] %channel%.%level_name%: %message% %context.route%\n";
  // $formatter = new LineFormatter($output);

  // $streamHandler = new StreamHandler('php://stdout');
  // $streamHandler->setFormatter($formatter);
  // $logger->pushHandler($streamHandler);

// --------------------

  // JSON log
  //Create the formatter
  $formatter = new JsonFormatter();

  //Set the formatter
  $handler = new StreamHandler(change_file_extension($logFile, '.json'), Logger::DEBUG);
  $handler->setFormatter($formatter);

  //Set the handler
  $logger->pushHandler($handler);


  return $logger;
}