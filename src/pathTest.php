<?php

namespace pruga\utils;

require_once __DIR__ . "/path.php";

// // define("WORKING_DIRECTORY", getcwd());
// define("WORKING_DIRECTORY", '.');

// $output = [ WORKING_DIRECTORY . '/pages/admin/users', 
// WORKING_DIRECTORY . '/pages/admin',
// WORKING_DIRECTORY . '/pages',
// WORKING_DIRECTORY];



// test('nadřazené adresáře relativní cestou', function () {

//     global $output;

//     $paths = array();

//     // foreach (to_parents($path = "./pages/admin/users/", $root = "/pruga/devel/pruga-web/") as $key => $value) {
//     foreach (paths2root("./pages/admin/users/") as $key => $value) { 
//         $paths[] = $value;

//         // , $root = "/pruga/devel/pruga-web/"
        
//     };

//     $this->assertEquals($output, $paths);

// });

// // test('nadřazené adresáře absolutní cestou', function () {

// //     global $output;

// //     $paths = array();

// //     foreach (paths2root("/pruga/devel/pruga-web/pages/admin/users") as $key => $value) {
        
// //         $paths[] = $value;

// //     };

// //     $this->assertEquals($output, $paths);

// // });
 
// // or
// // it('has home', function () {
// //     // ..
// // });

test('cesty', function () {

    $paths = array();

    // $output = find_parent(getcwd(), "pages/admin/users", 'layout.phtml');

    // $this->assertEquals($output, 'pages/layout.phtml');

    // it('throws exception', function () {
    //     path();
    // })->throws(ArgumentCountError::class);

    // $this->assertEquals(path('A'), 'A');
     
    $this->assertEquals(path("A", "B"), 'A/B');
    $this->assertEquals(path("", "B"), 'B');
    $this->assertEquals(path("/", "A"), '/A');
    $this->assertEquals(path("/A", "B"), '/A/B');
    $this->assertEquals(path("A", "", "C"), 'A/C');
    $this->assertEquals(path("A", "", "C/"), 'A/C');
    $this->assertEquals(path("A", "/", "C/"), 'A/C');
    $this->assertEquals(path("A", "", "C/", "/D/"), 'A/C/D');
    
    // foreach (paths2root("/A/B/C/D/E/") as $path){
    //     echo $path;
    // }
});

const TEST_DATA_DIR = __DIR__ . "/../tests/data";

test('glob rekurzivní soubory', function () {

    $iterator = glob_recursive_files(TEST_DATA_DIR, '*');
    $paths = iterator_to_array($iterator);
    $paths = toRelativePaths($paths, TEST_DATA_DIR);
    $paths = iterator_to_array($paths);
    
    $this->assertFalse(in_array('data', $paths));
    $this->assertFalse(in_array('A/B', $paths));
    $this->assertTrue(in_array("/A/B/b.php", $paths));
    $this->assertTrue(in_array('/A/B/b.py', $paths));
    $this->assertTrue(in_array('/A/B/C/c.html', $paths));

    $iterator = glob_recursive_files(TEST_DATA_DIR, '*.{md,html}', GLOB_BRACE | GLOB_ERR);
    $paths = iterator_to_array($iterator);
    $paths = toRelativePaths($paths, TEST_DATA_DIR);
    $paths = iterator_to_array($paths);

    $this->assertFalse(in_array("/A/B/b.php", $paths));
    $this->assertFalse(in_array('/A/B/b.py', $paths));
    $this->assertTrue(in_array('/A/B/C/c.html', $paths));

    
    

    
});

test('glob rekurzivní adresáře', function () {

    $iterator = glob_recursive_dirs(TEST_DATA_DIR);
    // $paths = iterator_to_array($iterator);
    $paths = array();
    foreach (glob_recursive_dirs(TEST_DATA_DIR) as $key => $value) {
        $paths[] = $value;
      }
    
    $paths = toRelativePaths($paths, TEST_DATA_DIR);
    $paths = iterator_to_array($paths);
    // var_dump($paths);
    // exit();
    $this->assertTrue(in_array("/A", $paths));
    $this->assertTrue(in_array("/A/B", $paths));
    $this->assertTrue(in_array("/A/B/C", $paths));
    $this->assertTrue(in_array("/AA", $paths));

    
});

test('paths2root, cesty ke kořenu', function () {

    // $iterator = glob_recursive_dirs(path("pages"));
    // $paths = iterator_to_array($iterator);
    // $this->assertTrue(in_array("pages/", $paths));
    // $this->assertTrue(in_array("pages/blog", $paths));
    // $this->assertTrue(in_array("pages/blog/markdown", $paths));

    // $this->assertFalse(in_array("pages/index.md", $paths));
    // $this->assertFalse(in_array("pages/blog/index.html", $paths));
    // $this->assertFalse(in_array("pages/blog/markdown/index.md", $paths));

    // foreach (paths2root("/A/B/C/D") as $route) {
    //     echo "$route\n";
    // }
    $iterator = paths2root("/A/B/C/D");
    $paths = iterator_to_array($iterator);

    $this->assertEquals('/A/B/C/D', $paths[0]);
    $this->assertEquals('/', $paths[4]);
});

