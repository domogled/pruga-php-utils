<?php

namespace pruga\utils;

function get_server_request_method(): string {
    if(php_sapi_name() === 'cli'){
        return "CLI";
    }
    else {
        return $_SERVER['REQUEST_METHOD'];
    }
}


function get_server_path_info(): string{
    global $argc, $argv;
    
    // var_dump($_SERVER);

    if(php_sapi_name() === 'cli'){
        return $argv[1] ?? ''; //throw new PrugaCliException("Zadej název souboru v argumentu");
    }
    else {
        // return $_SERVER['REQUEST_URI'];
        return array_key_exists('PHP_SELF', $_SERVER) ? $_SERVER['PHP_SELF'] : '';
    }
}
