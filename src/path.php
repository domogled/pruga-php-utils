<?php
/**
 * toto je knihovní soubor, kde najdeš
 * pomocné funkce pro práci s řetězci
 * takové nějaké, na jaké jsme zvyklí v Pythonu a Nimu
 **/

namespace pruga\utils;
// require_once __DIR__ . "/exceptions.php";


 // od verze PHP 8 zbytečné
//  if(PHP_VERSION < 8)
// function startsWith($haystack, $needle)
// {
//     return substr_compare($haystack, $needle, 0, strlen($needle)) === 0;
// }
// function endsWith($haystack, $needle)
// {
//     return substr_compare($haystack, $needle, -strlen($needle)) === 0;
// }

// function check_realpath(string $path): string{
//     $realPath = \realpath($path);
//     if($realPath == false){
//         throw new \pruga\PrugaFileNotFoundException("Nejestvuje soubor $path");
//     }
//     return $realPath;
// }

function removeAll(string $path) {
  /** vymaže adresář včetně všeho jeho obsahu
   * k čemuž je nutná tato pomocná rekurzivní funkce
   * 
  */

  if (!is_dir($path)) {
    // throw new \InvalidArgumentException("$path musí býti adresářem");
    echo "Nemožu smazat adresář $path, bo to není adresář.\n";
  }

  if (is_dir($path)) {
   
    $subPaths = glob($path . '/*');
    foreach ($subPaths as $subPath) {
        is_dir($subPath) ? removeAll($subPath) : unlink($subPath);
    }
    rmdir($path);
  }
}

/**  proc relativePath(path, base: string; sep = DirSep): string {.....} */
function relativePath(string $path, string $base /*, $ps = DIRECTORY_SEPARATOR*/): string
{

    // $path = realpath($path);
    // $base = realpath($base);

    if (!str_starts_with($path, $base)) {
        throw new \InvalidArgumentException("Adresář '$path' není podadresářem v '$base'", 1);
    }


    return substr($path, strlen($base));

}

// function absolutePath($path) {
//   $path = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $path);
//   $parts = array_filter(explode(DIRECTORY_SEPARATOR, $path), 'strlen');
//   $absolutes = array();
//   foreach ($parts as $part) {
//       if ('.' == $part) continue;
//       if ('..' == $part) {
//           array_pop($absolutes);
//       } else {
//           $absolutes[] = $part;
//       }
//   }
//   return implode(DIRECTORY_SEPARATOR, $absolutes);
// }

function toRelativePaths(array $paths, string $base /*, $ps = DIRECTORY_SEPARATOR*/): \Generator
{

    foreach ($paths as $path) {
      yield relativePath(path: $path, base: $base);
    }

}

function glob_recursive_files(string $baseDir, string $pattern = "*", int $flags = 0): \Generator {
  /**
   * rekurzivně projde adresáře a vrací všechny soubory podle patternu funkcí glob
   */

  foreach(glob_recursive_dirs(dir: $baseDir) as $dir){

    foreach (glob(path($dir, $pattern), $flags) as /*$key =>*/ $name) {
      if (is_dir($name)) {
        continue;
      }
        yield $name;
    }

  }

  

  // foreach (glob(path($dir, '*'), GLOB_ONLYDIR | GLOB_ERR) as $subDir) {
  //   foreach(glob_recursive_files($subDir, $pattern, $flags) as $file){
  //     yield $file;
  //   }
  // }
    
}

function glob_recursive_dirs(string $dir): \Generator {
    /**
     * rekurzivně projde adresář a vrací všechny podadresáře
     */


  if (! is_dir($dir)) {
    throw new \InvalidArgumentException("$dir musí býti adresářem");
  }

  yield $dir;
  
  foreach (glob(path($dir, '*'), GLOB_ONLYDIR | GLOB_ERR) as $subDir) {
    
    yield from glob_recursive_dirs(dir: $subDir);
  }
    
}

function change_file_extension(string $filename, string $extension){
    $info = pathinfo($filename);
    return $info['dirname'] . DIRECTORY_SEPARATOR . $info['filename'] . '.' . ltrim($extension, '.');
}

function strip_path($path): string {
    return trim($path, DIRECTORY_SEPARATOR);
}

function path(string $firstArg, string ...$args): string{

    $args = array_map( __NAMESPACE__ . '\strip_path', $args);
    $args = array_filter($args, function($value) {
        return $value != "";  });

    $path = join(DIRECTORY_SEPARATOR, $args);

    if ($firstArg != '') {
        return rtrim($firstArg, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR .  $path;
    }
    else {
        return $path;
    }
}


// ***********************************************************************************

function find_parent(string $root, string $route, string $name):string{
    /** používám
     * 
    */
    
    $pathName = path($root, $route, $name);
    // echo "FIND " . $pathName . "\n";
    if(is_file($pathName)){
        return $pathName;
    }
    $routeInfo = pathinfo($route);

    if (array_key_exists('dirname', $routeInfo)) {
        # code...
    
    
        if($route != $routeInfo['dirname']){
            return find_parent(root: $root, route: $routeInfo['dirname'], name: $name);
            // 
        }
        
        
    }
    
    $defaultFile = path(TEMPLATES_DIRECTORY, $name);
    // echo $defaultFile;
    if (is_file($defaultFile)) {
        return $defaultFile;
    }


    throw new \Exception("Nejestvuje soubor $name v $pathName ani výchozí v $defaultFile");

}

function paths2root(string $path, string $root = ""): \Generator {

    while(true) {
        if ($root == "") {
          yield $path;
        }
        else {
          yield path($root, $path);
        }
        
        $dir = dirname($path);

        if ($dir == $path) {
          break;
        }
        $path = $dir;
        
    }

}
        
    


// function paths2root(string $path, string $root) {

//   foreach (paths2root($path) as $path) {
//       yield path($root, $path);
//   }

// }